
//........Console...........//
console.log(26);
console.log(99);

//........Comments...........//
//1)
//Opening line

console.log('It was love at first sight.');

/*console.log('The first time Yossarian saw the chaplain he fell madly in love with him.');
console.log('Yossarian was in the hospital with a pain in his liver that fell just short of being jaundice.');
console.log('The doctors were puzzled by the fact that it wasn\'t quite jaundice.');
console.log('If it became jaundice they could treat it.');
console.log('If it didn\'t become jaundice and went away they could discharge him.');
console.log('But this just being short of jaundice all the time confused them.');*/
//2)
//Opening line

console.log('It was love at first sight.');
/*
console.log('The first time Yossarian saw the chaplain he fell madly in love with him.');
console.log('Yossarian was in the hospital with a pain in his liver that fell just short of being jaundice.');
console.log('The doctors were puzzled by the fact that it wasn\'t quite jaundice.');
console.log('If it became jaundice they could treat it.');
console.log('If it didn\'t become jaundice and went away they could discharge him.');
console.log('But this just being short of jaundice all the time confused them.');
*/
//........Data Types...........//
//1
console.log('JavaScript');
//2
console.log(2011);
//3
console.log('Woohoo! I love to code! #codecademy');
//4
console.log(20.49);

//........Arithmetic Operators...........//
//1
console.log(26 + 3.5);
//2
console.log(2021-1969);
//3
console.log(65/240);
//4
console.log(0.2708 * 100);

//........String Concatenation...........//
//1
console.log( 'Hello' + 'World');
//2
console.log('Hello' + ' ' + 'World');

//........Properties...........//
//1
console.log('Teaching the world how to code'.length);


//........Methonds...........//
// Use .toUpperCase() to log 'Codecademy' in all uppercase letters
console.log('Codecademy'.toUpperCase());

// Use a string method to log the following string without whitespace at the beginning and end of it.
console.log('    Remove whitespace   '.trim());


//........Built-in-Objects...........//
//1
console.log(Math.random() * 100);
//2
console.log(Math.floor(Math.random() * 100));
//3
console.log(Math.ceil(43.8));
//4
console.log(Number.isInteger(2017));



//........Create a Variable: var...........//
//1
var favoriteFood = 'pizza';
//2
var numOfSlices = 8;
//3
console.log(favoriteFood);
console.log(numOfSlices);

//........Create a Variable: let...........//
//1
let changeMe = true;
//2
changeMe = false;
console.log(changeMe);

//........Create a Variable: const...........//
//1
const entree = 'Enchiladas';
//2
console.log(entree);
//3
entree = 'Tacos';



//........Mathematical Assingment  Operators...........//
//1
let levelUp = 10;
let powerLevel = 9001;
let multiplyMe = 32;
let quarterMe = 1152;

// Use the mathematical assignments in the space below:

levelUp += 5;
powerLevel -= 100;
multiplyMe *= 11;
quarterMe /= 4;

// These console.log() statements below will help you check the values of the variables.
// You do not need to edit these statements. 
console.log('The value of levelUp:', levelUp); 
console.log('The value of powerLevel:', powerLevel); 
console.log('The value of multiplyMe:', multiplyMe); 
console.log('The value of quarterMe:', quarterMe);


//........The  Increment and Decrement  Operators...........//
let gainedDollar = 3;
let lostDollar = 50;

gainedDollar++;
lostDollar--;

console.log(gainedDollar);
console.log(lostDollar);



//........String Concatenation with Variables...........//
var favoriteAnimal = 'pet';
console.log('My favorite animal: ' + favoriteAnimal );


//........String Interpolation...........//
var myName = 'seyoung';
let myCity = 'seoul';

console.log(`My name is ${myName}. My favorite city is ${myCity}`);


//........typeof Operator..........//
let newVariable = 'Playing around with typeof.';

console.log(typeof newVariable);

newVariable = 1;
console.log(typeof newVariable);